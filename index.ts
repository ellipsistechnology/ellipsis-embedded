'use strict';

function verbose(log: any) {
  console.log(log)
}

import {factory, callServices, taggedService} from 'blackbox-ioc'
import {DefaultRuleBase} from 'blackbox-rules-utils'
import RuleBase from 'blackbox-rules'
import cors from 'cors'
import {v4 as uuidv4} from 'uuid'
import {Agent} from 'blackbox-discovery'

const DEFAULT_PATH = process.env.HOME+'/.blackbox'
const CONFIG_NAME = 'blackbox-conf.json'
const CONFIG_PATHS = [DEFAULT_PATH+'/'+CONFIG_NAME, '/etc/'+CONFIG_NAME]
const DEFAULT_PORT = 8100

process
  .on('unhandledRejection', (reason, p) => {
    console.error(reason, 'Unhandled Rejection at Promise', p);
  })
  .on('uncaughtException', err => {
    console.error(err, 'Uncaught Exception thrown');
  });

var fs = require('fs'),
    path = require('path'),
    https = require('https'),
    http = require('http'),
    glob = require('glob'),
    path = require('path'),
    app = require('connect')(),
    oas3Tools = require('oas3-tools'),
    jsyaml = require('js-yaml');

// Load all files to ensure annotations are read:
glob
  .sync(`${__dirname}/*src/**/*.js`)
  .forEach( (file:string) => {
    require(path.resolve(file))
  } )

// The Swagger document (require it, build it programmatically, fetch it from a URL, ...)
var spec = fs.readFileSync(path.join(__dirname,'api/openapi.yaml'), 'utf8');
var swaggerDoc = jsyaml.safeLoad(spec);

// Read or setup config:
let config: any = undefined
for(let strPath of CONFIG_PATHS) {
  const p = path.resolve(strPath)
  verbose(`looking for config in path ${p}`)
  if(fs.existsSync(p)) {
    verbose(`found config...`)
    config = JSON.parse(fs.readFileSync(p).toString())
    if(config) {
      verbose(config)
      if(!config.agent) {
        throw new Error(`Blackbox config found at path '${strPath}' but agent element not found.`)
      }
      
      if(process.argv[2]) {
        config.agent.name = process.argv[2]
      } else if(!config.agent.name) {
        throw new Error(`Blackbox config found at path '${strPath}' but agent name not found.`)
      }

      if(process.argv[3]) {
        config.agent.port = parseInt(process.argv[3])
      } else if(!config.agent.port) {
        config.agent.port = DEFAULT_PORT
      }

      break // found a config, don't need to look for others
    } else {
      verbose(`failed to load config from '${strPath}`)
    }
  }
}

// Create default config:
if(!config) {
  verbose(`config not found; creating default config:`)
  config = {
    agent:{
      name: process.argv[2] ?? uuidv4(),
      port: process.argv[3] ? parseInt(process.argv[3]) : DEFAULT_PORT
    }
  }
  verbose(config)
  fs.mkdirSync(path.resolve(DEFAULT_PATH), {recursive: true})
  const configPath = path.resolve(DEFAULT_PATH+'/'+CONFIG_NAME)
  fs.writeFileSync(configPath, JSON.stringify(config, null, 2))
  verbose(`config created at path ${configPath}`)
}

// Initialisation:
class _InitIoc {
  @factory('rulebase')
  createRuleBase():RuleBase {
    return new DefaultRuleBase()
  }

  @factory('oasDoc')
  getOasDoc() {
    return swaggerDoc
  }

  @factory('blackbox-config')
  getConfig() {
    return config
  }

  @factory('agent')
  createAgent() {
    const agent = new Agent({
      name: config.agent.name,
      path: "/",
      port: config.agent.port,
      protocol: config.agent.protocol ?? 'http'
    })
    agent.startAnnouncing()
    return agent
  }

  @taggedService('init', 'init-server') 
  initServer() {
    // swaggerRouter configuration
    var options = {
      swaggerUi: path.join(__dirname, '/openapi.json'),
      controllers: path.join(__dirname, './gensrc/controllers'),
      useStubs: process.env.NODE_ENV === 'development' // Conditionally turn on stubs (mock mode)
    };

    // Initialize the Swagger middleware
    oas3Tools.initializeMiddleware(swaggerDoc, function (middleware:any) {

      app.use(cors());

      // Interpret Swagger resources and attach metadata to request - must be first in swagger-tools middleware chain
      app.use(middleware.swaggerMetadata());

      // Validate Swagger requests
      app.use(middleware.swaggerValidator());

      // Route validated requests to appropriate controller
      app.use(middleware.swaggerRouter(options));

      // Serve the Swagger documents and Swagger UI
      app.use(middleware.swaggerUi());

      // Start the server
      ( config.agent.tls ?
        https.createServer({
          key:  fs.readFileSync(config.agent.tls.key),
          cert: fs.readFileSync(config.agent.tls.cert),
          ca:   config.agent.tls.ca ? fs.readFileSync(config.agent.tls.ca) : undefined,
        }, app) :
        http.createServer(app)
      ).listen(config.agent.port, function () {
        console.log(`Your server is listening on port ${config.agent.port} (${config.agent.tls ? 'https' : 'http'}://localhost:${config.agent.port})`);
        console.log(`Swagger-ui is available on ${config.agent.tls ? 'https' : 'http'}://localhost:${config.agent.port}/docs`);
      });

    });
  }
}

import initRulesService from 'blackbox-rules-service'
initRulesService()

import initRootServices from 'blackbox-root-service'
initRootServices()

callServices('init')