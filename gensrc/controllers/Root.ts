'use strict';

var utils = require('../utils/writer.js');
import { autowiredService } from 'blackbox-ioc'
import { trimToDepth, verboseResponse } from 'blackbox-services';

class RootServiceWrapper {
  @autowiredService('root-service')
  service: any
}
const rootWrapper = new RootServiceWrapper()

function strip__(target: any): any {
  if(Array.isArray(target)) {
    return target.map(strip__)
  } else if(target && typeof target === 'object')  {
    const ret:any = {}
    Object.keys(target).forEach(key => {
      if(!key.startsWith('__'))
        ret[key] = target[key]
    })
    return ret
  } else {
    return target
  }
}

function convertBooleanParameter(val:any):boolean {
  if(val === 'false')
       return false;
  if(val === '')
     return true;
  return val
}

// 
// var Root = require('../service/RootService');
// 

module.exports.getRootService = function getRootService (req:any, res:any, next:any) {

  try {
    Promise.resolve(rootWrapper.service.getRootService({ req }))
    .then((val: any) => {
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};
