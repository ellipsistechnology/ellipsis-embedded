'use strict';

var utils = require('../utils/writer.js');
import { autowiredService } from 'blackbox-ioc'
import { trimToDepth, verboseResponse } from 'blackbox-services';

class EmbeddedServiceWrapper {
  @autowiredService('embedded-service')
  service: any
}
const embeddedWrapper = new EmbeddedServiceWrapper()

function strip__(target: any): any {
  if(Array.isArray(target)) {
    return target.map(strip__)
  } else if(target && typeof target === 'object')  {
    const ret:any = {}
    Object.keys(target).forEach(key => {
      if(!key.startsWith('__'))
        ret[key] = target[key]
    })
    return ret
  } else {
    return target
  }
}

function convertBooleanParameter(val:any):boolean {
  if(val === 'false')
       return false;
  if(val === '')
     return true;
  return val
}

// 
// var Embedded = require('../service/EmbeddedService');
// 

module.exports.getEmbedded = function getEmbedded (req:any, res:any, next:any) {
  var name = req.swagger.params['name'].value;
  var verbose = req.swagger.params['verbose'].value;
  var depth = req.swagger.params['depth'].value;
  verbose = convertBooleanParameter(verbose)

  try {
    Promise.resolve(embeddedWrapper.service.getEmbedded({ req, name, verbose, depth }))
    .then((val: any) => {
      val = verboseResponse(val, verbose)
      val = trimToDepth(val, depth)
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};


module.exports.getEmbeddedList = function getEmbeddedList (req:any, res:any, next:any) {
  var depth = req.swagger.params['depth'].value;
  var verbose = req.swagger.params['verbose'].value;
  verbose = convertBooleanParameter(verbose)

  try {
    Promise.resolve(embeddedWrapper.service.getEmbeddedList({ req, depth, verbose }))
    .then((val: any) => {
      val = verboseResponse(val, verbose)
      val = trimToDepth(val, depth)
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};


module.exports.getOptionsForEmbedded = function getOptionsForEmbedded (req:any, res:any, next:any) {
  var name = req.swagger.params['name'].value;
  var verbose = req.swagger.params['verbose'].value;
  var depth = req.swagger.params['depth'].value;
  verbose = convertBooleanParameter(verbose)

  try {
    Promise.resolve(embeddedWrapper.service.getOptionsForEmbedded({ req, name, verbose, depth }))
    .then((val: any) => {
      val = verboseResponse(val, verbose)
      val = trimToDepth(val, depth)
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};


module.exports.getOptionsForEmbeddedService = function getOptionsForEmbeddedService (req:any, res:any, next:any) {
  var depth = req.swagger.params['depth'].value;
  var verbose = req.swagger.params['verbose'].value;
  verbose = convertBooleanParameter(verbose)

  try {
    Promise.resolve(embeddedWrapper.service.getOptionsForEmbeddedService({ req, depth, verbose }))
    .then((val: any) => {
      val = verboseResponse(val, verbose)
      val = trimToDepth(val, depth)
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};


module.exports.replaceEmbedded = function replaceEmbedded (req:any, res:any, next:any) {
  var name = req.swagger.params['name'].value;
  var verbose = req.swagger.params['verbose'].value;
  var embeddedDevice = req.body;
  verbose = convertBooleanParameter(verbose)

  try {
    Promise.resolve(embeddedWrapper.service.replaceEmbedded({ req, name, verbose, embeddedDevice }))
    .then((val: any) => {
      val = verboseResponse(val, verbose)
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};


module.exports.updateEmbedded = function updateEmbedded (req:any, res:any, next:any) {
  var name = req.swagger.params['name'].value;
  var verbose = req.swagger.params['verbose'].value;
  var embeddedDevice = req.body;
  verbose = convertBooleanParameter(verbose)

  try {
    Promise.resolve(embeddedWrapper.service.updateEmbedded({ req, name, verbose, embeddedDevice }))
    .then((val: any) => {
      val = verboseResponse(val, verbose)
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};
