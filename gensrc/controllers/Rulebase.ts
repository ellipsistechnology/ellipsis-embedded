'use strict';

var utils = require('../utils/writer.js');
import { autowiredService } from 'blackbox-ioc'
import { trimToDepth, verboseResponse } from 'blackbox-services';

class RulebaseServiceWrapper {
  @autowiredService('rulebase-service')
  service: any
}
const rulebaseWrapper = new RulebaseServiceWrapper()

function strip__(target: any): any {
  if(Array.isArray(target)) {
    return target.map(strip__)
  } else if(target && typeof target === 'object')  {
    const ret:any = {}
    Object.keys(target).forEach(key => {
      if(!key.startsWith('__'))
        ret[key] = target[key]
    })
    return ret
  } else {
    return target
  }
}

function convertBooleanParameter(val:any):boolean {
  if(val === 'false')
       return false;
  if(val === '')
     return true;
  return val
}

// 
// var Rulebase = require('../service/RulebaseService');
// 

module.exports.createCondition = function createCondition (req:any, res:any, next:any) {
  var verbose = req.swagger.params['verbose'].value;
  var condition = req.body;
  verbose = convertBooleanParameter(verbose)

  try {
    Promise.resolve(rulebaseWrapper.service.createCondition({ req, verbose, condition }))
    .then((val: any) => {
      val = verboseResponse(val, verbose)
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};


module.exports.createRule = function createRule (req:any, res:any, next:any) {
  var verbose = req.swagger.params['verbose'].value;
  var rule = req.body;
  verbose = convertBooleanParameter(verbose)

  try {
    Promise.resolve(rulebaseWrapper.service.createRule({ req, verbose, rule }))
    .then((val: any) => {
      val = verboseResponse(val, verbose)
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};


module.exports.createValue = function createValue (req:any, res:any, next:any) {
  var verbose = req.swagger.params['verbose'].value;
  var value = req.body;
  verbose = convertBooleanParameter(verbose)

  try {
    Promise.resolve(rulebaseWrapper.service.createValue({ req, verbose, value }))
    .then((val: any) => {
      val = verboseResponse(val, verbose)
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};


module.exports.deleteCondition = function deleteCondition (req:any, res:any, next:any) {
  var name = req.swagger.params['name'].value;

  try {
    Promise.resolve(rulebaseWrapper.service.deleteCondition({ req, name }))
    .then((val: any) => {
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};


module.exports.deleteRule = function deleteRule (req:any, res:any, next:any) {
  var name = req.swagger.params['name'].value;

  try {
    Promise.resolve(rulebaseWrapper.service.deleteRule({ req, name }))
    .then((val: any) => {
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};


module.exports.deleteValue = function deleteValue (req:any, res:any, next:any) {
  var name = req.swagger.params['name'].value;

  try {
    Promise.resolve(rulebaseWrapper.service.deleteValue({ req, name }))
    .then((val: any) => {
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};


module.exports.getCondition = function getCondition (req:any, res:any, next:any) {
  var name = req.swagger.params['name'].value;
  var verbose = req.swagger.params['verbose'].value;
  var depth = req.swagger.params['depth'].value;
  verbose = convertBooleanParameter(verbose)

  try {
    Promise.resolve(rulebaseWrapper.service.getCondition({ req, name, verbose, depth }))
    .then((val: any) => {
      val = verboseResponse(val, verbose)
      val = trimToDepth(val, depth)
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};


module.exports.getConditions = function getConditions (req:any, res:any, next:any) {
  var verbose = req.swagger.params['verbose'].value;
  var depth = req.swagger.params['depth'].value;
  verbose = convertBooleanParameter(verbose)

  try {
    Promise.resolve(rulebaseWrapper.service.getConditions({ req, verbose, depth }))
    .then((val: any) => {
      val = verboseResponse(val, verbose)
      val = trimToDepth(val, depth)
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};


module.exports.getRule = function getRule (req:any, res:any, next:any) {
  var name = req.swagger.params['name'].value;
  var verbose = req.swagger.params['verbose'].value;
  var depth = req.swagger.params['depth'].value;
  verbose = convertBooleanParameter(verbose)

  try {
    Promise.resolve(rulebaseWrapper.service.getRule({ req, name, verbose, depth }))
    .then((val: any) => {
      val = verboseResponse(val, verbose)
      val = trimToDepth(val, depth)
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};


module.exports.getRulebaseService = function getRulebaseService (req:any, res:any, next:any) {

  try {
    Promise.resolve(rulebaseWrapper.service.getRulebaseService({ req }))
    .then((val: any) => {
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};


module.exports.getRules = function getRules (req:any, res:any, next:any) {
  var verbose = req.swagger.params['verbose'].value;
  var depth = req.swagger.params['depth'].value;
  verbose = convertBooleanParameter(verbose)

  try {
    Promise.resolve(rulebaseWrapper.service.getRules({ req, verbose, depth }))
    .then((val: any) => {
      val = verboseResponse(val, verbose)
      val = trimToDepth(val, depth)
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};


module.exports.getValue = function getValue (req:any, res:any, next:any) {
  var name = req.swagger.params['name'].value;
  var verbose = req.swagger.params['verbose'].value;
  var depth = req.swagger.params['depth'].value;
  verbose = convertBooleanParameter(verbose)

  try {
    Promise.resolve(rulebaseWrapper.service.getValue({ req, name, verbose, depth }))
    .then((val: any) => {
      val = verboseResponse(val, verbose)
      val = trimToDepth(val, depth)
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};


module.exports.getValues = function getValues (req:any, res:any, next:any) {
  var verbose = req.swagger.params['verbose'].value;
  var depth = req.swagger.params['depth'].value;
  verbose = convertBooleanParameter(verbose)

  try {
    Promise.resolve(rulebaseWrapper.service.getValues({ req, verbose, depth }))
    .then((val: any) => {
      val = verboseResponse(val, verbose)
      val = trimToDepth(val, depth)
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};


module.exports.replaceCondition = function replaceCondition (req:any, res:any, next:any) {
  var name = req.swagger.params['name'].value;
  var verbose = req.swagger.params['verbose'].value;
  var condition = req.body;
  verbose = convertBooleanParameter(verbose)

  try {
    Promise.resolve(rulebaseWrapper.service.replaceCondition({ req, name, verbose, condition }))
    .then((val: any) => {
      val = verboseResponse(val, verbose)
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};


module.exports.replaceRule = function replaceRule (req:any, res:any, next:any) {
  var name = req.swagger.params['name'].value;
  var verbose = req.swagger.params['verbose'].value;
  var rule = req.body;
  verbose = convertBooleanParameter(verbose)

  try {
    Promise.resolve(rulebaseWrapper.service.replaceRule({ req, name, verbose, rule }))
    .then((val: any) => {
      val = verboseResponse(val, verbose)
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};


module.exports.replaceValue = function replaceValue (req:any, res:any, next:any) {
  var name = req.swagger.params['name'].value;
  var verbose = req.swagger.params['verbose'].value;
  var value = req.body;
  verbose = convertBooleanParameter(verbose)

  try {
    Promise.resolve(rulebaseWrapper.service.replaceValue({ req, name, verbose, value }))
    .then((val: any) => {
      val = verboseResponse(val, verbose)
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};


module.exports.updateCondition = function updateCondition (req:any, res:any, next:any) {
  var name = req.swagger.params['name'].value;
  var verbose = req.swagger.params['verbose'].value;
  var condition = req.body;
  verbose = convertBooleanParameter(verbose)

  try {
    Promise.resolve(rulebaseWrapper.service.updateCondition({ req, name, verbose, condition }))
    .then((val: any) => {
      val = verboseResponse(val, verbose)
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};


module.exports.updateRule = function updateRule (req:any, res:any, next:any) {
  var name = req.swagger.params['name'].value;
  var verbose = req.swagger.params['verbose'].value;
  var rule = req.body;
  verbose = convertBooleanParameter(verbose)

  try {
    Promise.resolve(rulebaseWrapper.service.updateRule({ req, name, verbose, rule }))
    .then((val: any) => {
      val = verboseResponse(val, verbose)
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};


module.exports.updateValue = function updateValue (req:any, res:any, next:any) {
  var name = req.swagger.params['name'].value;
  var verbose = req.swagger.params['verbose'].value;
  var value = req.body;
  verbose = convertBooleanParameter(verbose)

  try {
    Promise.resolve(rulebaseWrapper.service.updateValue({ req, name, verbose, value }))
    .then((val: any) => {
      val = verboseResponse(val, verbose)
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};
