'use strict';

var utils = require('../utils/writer.js');
import { autowiredService } from 'blackbox-ioc'
import { trimToDepth, verboseResponse } from 'blackbox-services';

class ScheduleServiceWrapper {
  @autowiredService('schedule-service')
  service: any
}
const scheduleWrapper = new ScheduleServiceWrapper()

function strip__(target: any): any {
  if(Array.isArray(target)) {
    return target.map(strip__)
  } else if(target && typeof target === 'object')  {
    const ret:any = {}
    Object.keys(target).forEach(key => {
      if(!key.startsWith('__'))
        ret[key] = target[key]
    })
    return ret
  } else {
    return target
  }
}

function convertBooleanParameter(val:any):boolean {
  if(val === 'false')
       return false;
  if(val === '')
     return true;
  return val
}

// 
// var Schedule = require('../service/ScheduleService');
// 

module.exports.createSchedule = function createSchedule (req:any, res:any, next:any) {
  var verbose = req.swagger.params['verbose'].value;
  var schedule = req.body;
  verbose = convertBooleanParameter(verbose)

  try {
    Promise.resolve(scheduleWrapper.service.createSchedule({ req, verbose, schedule }))
    .then((val: any) => {
      val = verboseResponse(val, verbose)
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};


module.exports.deleteSchedule = function deleteSchedule (req:any, res:any, next:any) {
  var name = req.swagger.params['name'].value;

  try {
    Promise.resolve(scheduleWrapper.service.deleteSchedule({ req, name }))
    .then((val: any) => {
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};


module.exports.getOptionsForSchedule = function getOptionsForSchedule (req:any, res:any, next:any) {
  var name = req.swagger.params['name'].value;
  var verbose = req.swagger.params['verbose'].value;
  var depth = req.swagger.params['depth'].value;
  verbose = convertBooleanParameter(verbose)

  try {
    Promise.resolve(scheduleWrapper.service.getOptionsForSchedule({ req, name, verbose, depth }))
    .then((val: any) => {
      val = verboseResponse(val, verbose)
      val = trimToDepth(val, depth)
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};


module.exports.getOptionsForScheduleService = function getOptionsForScheduleService (req:any, res:any, next:any) {
  var depth = req.swagger.params['depth'].value;
  var verbose = req.swagger.params['verbose'].value;
  verbose = convertBooleanParameter(verbose)

  try {
    Promise.resolve(scheduleWrapper.service.getOptionsForScheduleService({ req, depth, verbose }))
    .then((val: any) => {
      val = verboseResponse(val, verbose)
      val = trimToDepth(val, depth)
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};


module.exports.getSchedule = function getSchedule (req:any, res:any, next:any) {
  var name = req.swagger.params['name'].value;
  var verbose = req.swagger.params['verbose'].value;
  var depth = req.swagger.params['depth'].value;
  verbose = convertBooleanParameter(verbose)

  try {
    Promise.resolve(scheduleWrapper.service.getSchedule({ req, name, verbose, depth }))
    .then((val: any) => {
      val = verboseResponse(val, verbose)
      val = trimToDepth(val, depth)
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};


module.exports.getScheduleList = function getScheduleList (req:any, res:any, next:any) {
  var depth = req.swagger.params['depth'].value;
  var verbose = req.swagger.params['verbose'].value;
  verbose = convertBooleanParameter(verbose)

  try {
    Promise.resolve(scheduleWrapper.service.getScheduleList({ req, depth, verbose }))
    .then((val: any) => {
      val = verboseResponse(val, verbose)
      val = trimToDepth(val, depth)
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};


module.exports.replaceSchedule = function replaceSchedule (req:any, res:any, next:any) {
  var name = req.swagger.params['name'].value;
  var verbose = req.swagger.params['verbose'].value;
  var schedule = req.body;
  verbose = convertBooleanParameter(verbose)

  try {
    Promise.resolve(scheduleWrapper.service.replaceSchedule({ req, name, verbose, schedule }))
    .then((val: any) => {
      val = verboseResponse(val, verbose)
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};


module.exports.updateSchedule = function updateSchedule (req:any, res:any, next:any) {
  var name = req.swagger.params['name'].value;
  var verbose = req.swagger.params['verbose'].value;
  var schedule = req.body;
  verbose = convertBooleanParameter(verbose)

  try {
    Promise.resolve(scheduleWrapper.service.updateSchedule({ req, name, verbose, schedule }))
    .then((val: any) => {
      val = verboseResponse(val, verbose)
      val = strip__(val)

      if(req.swagger &&
        req.swagger.operation &&
        req.swagger.operation.responses &&
        req.swagger.operation.responses['200'] &&
        req.swagger.operation.responses['200']['x-content-blackbox-type']
      ) {
        const contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type']
        res.setHeader('content-blackbox-type', contentBlackboxType)
      }

      utils.writeJson(res, val)
    }).catch((err: any) => {
      next(err);
    })
  } catch(err) {
    next(err)
  }
};
