import {serviceClass, autowired} from 'blackbox-ioc'
import {makeServiceObject} from 'blackbox-services'
import Schedule from './Schedule'
import {Service} from 'blackbox-services'
import {NamedReference} from 'blackbox-services'
import fs from 'fs'
import axios from 'axios'
import Event from 'Event'
import HttpRequest from 'HttpRequest'

const https = require('https')

const DATA_PATH = "scheduleData.json"

const httpEvent = (httpRequest: HttpRequest, config: any) => () => {
  let agent = httpRequest.url?.toLowerCase()?.startsWith('https') ?
    new https.Agent({
      rejectUnauthorized: false // TODO: Replace this with a ca property pointing to trusted certificates.
    }) : 
    undefined
  axios.request({
    method: <any>httpRequest.method,
    url: httpRequest.url,
    headers: Object.assign({ 'content-type': 'application/json' }, (httpRequest.headers)),
    data: httpRequest.body,
    httpsAgent: agent
  })
  .then(_response => console.log(`Event sent to ${httpRequest.url}`))
  .catch(error => {
    console.error(`Error sending ${httpRequest.method} request to ${httpRequest.url}`);
    console.error(error)
  })
}

@serviceClass('schedule-service')
export class ScheduleServiceImpl {

  @autowired('oasDoc')
  oasDoc:any

  @autowired('blackbox-config')
  config: any

  data: Schedule[] = []

  eventHandles: {[key: string]: any[]} = {}

  constructor() {
    if(fs.existsSync(DATA_PATH)) {
      try {
        this.data = <Schedule[]><any>JSON.parse(fs.readFileSync(DATA_PATH).toString())
        this.data.forEach(s => this.scheduleEvents(s))
      }
      catch(err) {
        console.error(err)
      }
    }
  }

  private writeData() {
    fs.promises.writeFile(DATA_PATH, JSON.stringify(this.data, null, 2))
    .catch(console.error)
  }

  private scheduleEvents(schedule: Schedule): void {
    this.eventHandles[schedule.name] = []
    schedule.events?.forEach(event => {
      const endTime = new Date(schedule.end).getTime()
      const startTime = new Date(schedule.start).getTime()
      const now = new Date().getTime()
      let delay

      if(event.trigger === 'start') {
        // If end is in the future but start is in the past then trigger now:
        if(startTime < now && now < endTime) {
          delay = 0
        } else {
          delay = startTime - now
        }
      } else {
        delay = endTime - now
      }

      if(delay >= 0) {
        const eventFn = this.createEvent(event)
        if(eventFn) {
          this.eventHandles[schedule.name].push(setTimeout(eventFn, delay))
        }
      }
    })
  }

  private createEvent(event?: Event) {
    if (event?.type === 'http' && event.httpRequest) {
      return httpEvent(<HttpRequest>event.httpRequest, this.config)
    }
    return undefined
  }

  private cancelEvents(schedule: Schedule): void {
    // Cancel all schedules:
    this.eventHandles[schedule.name].forEach(clearTimeout)

    // Trigger schedule end if cancelling a schedule in progress:
    const now = new Date().getTime()
    const startTime = new Date(schedule.start).getTime()
    const endTime = new Date(schedule.end).getTime()
    if(startTime < now && now < endTime) {
      const eventFn = this.createEvent(schedule.events?.find(event => event.trigger === 'end'))
      if(eventFn) {
        eventFn()
      }
    }
  }

  private findSchedule(name: string): Schedule|undefined {
    const filtered = this.data.filter(s => s.name === name)
    if(filtered.length > 0) {
      return filtered[0]
    } else {
      return undefined
    }
  }
  
  getScheduleList(props:any):Schedule[] {
    return this.data
  }

  getOptionsForScheduleService(props:any):Service {
    return makeServiceObject(this.oasDoc, 'schedule')
  }

  createSchedule({schedule}:any):NamedReference {
    this.data.push(schedule)
    this.scheduleEvents(schedule)
    this.writeData()
    return {name: schedule.name}
  }

  getSchedule({name}:any):Schedule|undefined {
    return this.findSchedule(name)
  }

  getOptionsForSchedule(props:any):Service {
    return makeServiceObject(this.oasDoc, 'schedule')
  }

  replaceSchedule(props:any):NamedReference|undefined {
    return this.updateSchedule(props)
  }

  updateSchedule({schedule, name}:any):NamedReference|undefined {
    const s = this.findSchedule(name)
    if(s) {
      this.cancelEvents(s)
      Object.assign(s, schedule)
      this.scheduleEvents(s)
      this.writeData()
      return {name}
    }
    
    return undefined
  }

  deleteSchedule({name}:any):NamedReference|undefined {
    const schedule = this.findSchedule(name)
    if(schedule) {
      this.cancelEvents(schedule)

      for(let i = 0; i < this.data.length; ++i) {
        if(this.data[i].name === name) {
          this.data.splice(i, 1)
          this.writeData()
          return {name}
        }
      }
    }

    return undefined
  }
}
