import Event from './Event'

export default interface Schedule {
  name: string
  start: string
  end: string
  events?: Event[]
  description?: string
  username: string
}

