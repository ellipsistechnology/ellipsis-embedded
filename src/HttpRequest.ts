export default interface HttpRequest {
  method: string
  body: string
  url?: string
  headers?: {[key: string]: string}[]
}

