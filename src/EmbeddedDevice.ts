import { hierarchical } from 'blackbox-services'
import Programmer from './Programmer'

@hierarchical({path: 'embedded'})
export default class EmbeddedDevice {
  name: string
  status: string
  location?: string
  host?: string
  programmer?: Programmer
  type: string
  driver: {
    path: string
    onStart: string
    onStop: string
    getInfo: string
  }
  module?: any
  info?: any
  owner?: {
    username: string
  }
}

