import {serviceClass, autowired} from 'blackbox-ioc'
import {makeServiceObject} from 'blackbox-services'
import EmbeddedDevice from './EmbeddedDevice'
import {Service} from 'blackbox-services'
import {NamedReference} from 'blackbox-services'
import { Agent, AgentLike } from 'blackbox-discovery'
import {taggedService} from 'blackbox-ioc'
import {DataClient, ServiceClient} from 'blackbox-client'

@serviceClass('embedded-service')
export class EmbeddedServiceImpl {

  @autowired('oasDoc')
  oasDoc:any

  @autowired('agent')
  agent: Agent

  @autowired('blackbox-config')
  config: any

  devices: {[key: string]: EmbeddedDevice} = {}

  constructor() {
  }

  @taggedService('init', 'init-devices')
  initDevices() {
    try {
      this.config.devices?.forEach((device: any) => {
        if(device.name) {
          this.devices[device.name] = Object.assign(new EmbeddedDevice(), device)
          if(device.driver?.path) {
            import(device.driver.path)
            .then((m: any) => this.devices[device.name].module = m)
          }
        }
      });
    } catch(err) {
      console.error('Error initialising embedded service.')
      console.error(err)
    }
  }

  @taggedService('init', 'init-agent')
  initAgent() {
    this.agent.listeners.push(this.peerListener.bind(this))

    setInterval(() => Object.keys(this.devices).forEach(name => { // TODO: add time limit so we don't poll if recently updated, e.g. via a peer discovery
      if(this.devices[name].location === 'remote' && (<any>this.devices[name]).host) { // no need to poll ourself, and we need the host for the address
console.log(`setInterval: about to pollPeer(${(<any>this.devices[name]).host}, ${name})`);

        this.pollPeer((<any>this.devices[name]).host, name)
        .catch(err => {
          console.error(err)
          console.error(`Failed to connect to peer '${name}'`);
          delete this.devices[name]
        })
      }
    }), 60000)
  }
  
  private peerListener(peer: AgentLike) {
    // Only consider remote peers for which we know the address:
    if (!peer.address || peer.name === this.agent.name) {
      return
    }
console.log(`peerListener: about to pollPeer(${peer.protocol || 'http'}://${peer.address}:${peer.port}${peer.path})`);

    this.pollPeer(`${peer.protocol || 'http'}://${peer.address}:${peer.port}${peer.path}`)
    .catch(err => {
      console.error(err)
      console.error(`Failed to connect to peer '${peer.name}'`);
      delete this.devices[peer.name]
    })
  }

  private pollPeer(peerUrl: string, deviceName?: string) {
    return new ServiceClient(peerUrl).init()
    .then(client => {

      return client.navigateByType({
        name: 'embeddedDevice',
        uri: 'file:///opt/dev/ellipsis-embedded/datatype.json'
      })
    })
    .then(client => {
      if (!client) {
        return
      }

      if(deviceName) {
        return (<DataClient<EmbeddedDevice>>client).get(deviceName)
        .then(device => {
          if(!device) {
            delete this.devices[deviceName]
          } else {
            this.addDevices(<EmbeddedDevice>device, peerUrl)
          }
        })
      } else {
        return (<DataClient<EmbeddedDevice>>client).query('', true)
        .then((data: EmbeddedDevice[]) => data.forEach(device => this.addDevices(device, peerUrl)))
      }
    })
  }

  addDevices(device: EmbeddedDevice, rootUrl: string) {
    // Don't use a peer's description of ourself:
    if(this.devices[device.name]?.location === 'local') {
      return
    }
    
    this.devices[device.name] = Object.assign(new EmbeddedDevice(), {
      name: device.name,
      status: device.status,
      type: device.type,
      programmer: device.programmer,
      location: 'remote',
      host: rootUrl,
      info: device.info
    })

    // TODO: use a blackbox-service function to do this and all other __path references
    const path = (<any>this.devices[device.name]).__path
    if(path && !/^\w+:\/\//.test(path)) { // make sure it's absolute
      (<any>this.devices[device.name]).__path = rootUrl + path
    }
    

    // const links = (<any>device).links
    // if(links && links.self) {
    //   if(!(<any>this.devices[device.name]).links) {
    //     (<any>this.devices[device.name]).links = {}
    //   }

    //   if(/^\w+:\/\//.test(links.self.href)) { // Is the URL absolute?
    //     (<any>this.devices[device.name]).links.self = links.self
    //   } else {
    //     (<any>this.devices[device.name]).links.self = {
    //       href: rootUrl + links.self.href // Make the path absolute.
    //     }
    //   }
    // }
  }

  private updateInfo(device: EmbeddedDevice, req: any): EmbeddedDevice {
    const getInfo = device.module?.[device.driver?.getInfo ?? 'getInfo']
    if(typeof getInfo === 'function') {
      Promise.resolve(getInfo({req})).then(info => device.info = info)
    }
    return device
  }

  getEmbeddedList({req}:any):EmbeddedDevice[] {
    return Object.values(this.devices).map((device: EmbeddedDevice) => this.updateInfo(device, req))
  }

  getOptionsForEmbeddedService(props:any):Service {
    return makeServiceObject(this.oasDoc, 'embedded')
  }

  getEmbedded({name, req}:any):EmbeddedDevice|undefined {
    return this.updateInfo(this.devices[name], req)
  }

  getOptionsForEmbedded(props:any):Service {
    return makeServiceObject(this.oasDoc, 'embedded')
  }

  replaceEmbedded(props:any):NamedReference {
    return this.updateEmbedded(props)
  }

  private setDeviceError(device: EmbeddedDevice, err: any) {
    device.status = 'error'
    console.error(err)
  }

  updateEmbedded({name, embeddedDevice}:{name: string, embeddedDevice: EmbeddedDevice}):NamedReference {
    const device = this.devices[name]
    if(!device) {
      throw new Error(`Device with name ${name} not found.`)
    }

    if(device.location === 'remote') {
      throw new Error(`Cannot update remote device.`)
    }

    switch(embeddedDevice.status) {
    case "starting":
    case "running":
      if(device.status === "stopped") {
        const state = {owner: embeddedDevice.owner}
        console.log(`Starting device ${embeddedDevice.name} for owner ${embeddedDevice.owner?.username} with state ${JSON.stringify(state)}`) // TODO: Validate credentials.
        device.status = "starting"
        // Start device:
        if(typeof device.module?.[device.driver?.onStart ?? 'onStart'] === 'function') {
          Promise.resolve(device.module[device.driver?.onStart ?? 'onStart'](state))
          .then(() => device.status = 'running')
          .catch((err: any) => this.setDeviceError(device, err))
        }
      }
      break;
    case "stopped":
      if(device.status !== "stopped") {
        device.status = 'stopping'
        
        // Stop device:
        if(typeof device.module[device.driver?.onStop ?? 'onStop'] === 'function') {
          Promise.resolve(device.module[device.driver?.onStop ?? 'onStop']())
          .then(() => device.status = 'stopped')
          .catch((err: any) => this.setDeviceError(device, err))
        }

        // TODO: handle port with driver module and getInfo
        // if(device.programmer) {
        //   device.programmer.port = undefined
        // }
      }
      break;
    }

    return {name}
  }
}
