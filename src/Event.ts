import HttpRequest from './HttpRequest'

export default interface Event {
  type: string
  trigger: 'start'|'end'
  httpRequest?: HttpRequest
}

