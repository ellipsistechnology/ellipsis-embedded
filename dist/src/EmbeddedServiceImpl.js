"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EmbeddedServiceImpl = void 0;
var blackbox_ioc_1 = require("blackbox-ioc");
var blackbox_services_1 = require("blackbox-services");
var EmbeddedDevice_1 = __importDefault(require("./EmbeddedDevice"));
var blackbox_ioc_2 = require("blackbox-ioc");
var blackbox_client_1 = require("blackbox-client");
var EmbeddedServiceImpl = /** @class */ (function () {
    function EmbeddedServiceImpl() {
        this.devices = {};
    }
    EmbeddedServiceImpl.prototype.initDevices = function () {
        var _this = this;
        var _a;
        try {
            (_a = this.config.devices) === null || _a === void 0 ? void 0 : _a.forEach(function (device) {
                var _a;
                if (device.name) {
                    _this.devices[device.name] = Object.assign(new EmbeddedDevice_1.default(), device);
                    if ((_a = device.driver) === null || _a === void 0 ? void 0 : _a.path) {
                        Promise.resolve().then(function () { return __importStar(require(device.driver.path)); }).then(function (m) { return _this.devices[device.name].module = m; });
                    }
                }
            });
        }
        catch (err) {
            console.error('Error initialising embedded service.');
            console.error(err);
        }
    };
    EmbeddedServiceImpl.prototype.initAgent = function () {
        var _this = this;
        this.agent.listeners.push(this.peerListener.bind(this));
        setInterval(function () { return Object.keys(_this.devices).forEach(function (name) {
            if (_this.devices[name].location === 'remote' && _this.devices[name].host) { // no need to poll ourself, and we need the host for the address
                console.log("setInterval: about to pollPeer(" + _this.devices[name].host + ", " + name + ")");
                _this.pollPeer(_this.devices[name].host, name)
                    .catch(function (err) {
                    console.error(err);
                    console.error("Failed to connect to peer '" + name + "'");
                    delete _this.devices[name];
                });
            }
        }); }, 60000);
    };
    EmbeddedServiceImpl.prototype.peerListener = function (peer) {
        var _this = this;
        // Only consider remote peers for which we know the address:
        if (!peer.address || peer.name === this.agent.name) {
            return;
        }
        console.log("peerListener: about to pollPeer(" + (peer.protocol || 'http') + "://" + peer.address + ":" + peer.port + peer.path + ")");
        this.pollPeer((peer.protocol || 'http') + "://" + peer.address + ":" + peer.port + peer.path)
            .catch(function (err) {
            console.error(err);
            console.error("Failed to connect to peer '" + peer.name + "'");
            delete _this.devices[peer.name];
        });
    };
    EmbeddedServiceImpl.prototype.pollPeer = function (peerUrl, deviceName) {
        var _this = this;
        return new blackbox_client_1.ServiceClient(peerUrl).init()
            .then(function (client) {
            return client.navigateByType({
                name: 'embeddedDevice',
                uri: 'file:///opt/dev/ellipsis-embedded/datatype.json'
            });
        })
            .then(function (client) {
            if (!client) {
                return;
            }
            if (deviceName) {
                return client.get(deviceName)
                    .then(function (device) {
                    if (!device) {
                        delete _this.devices[deviceName];
                    }
                    else {
                        _this.addDevices(device, peerUrl);
                    }
                });
            }
            else {
                return client.query('', true)
                    .then(function (data) { return data.forEach(function (device) { return _this.addDevices(device, peerUrl); }); });
            }
        });
    };
    EmbeddedServiceImpl.prototype.addDevices = function (device, rootUrl) {
        var _a;
        // Don't use a peer's description of ourself:
        if (((_a = this.devices[device.name]) === null || _a === void 0 ? void 0 : _a.location) === 'local') {
            return;
        }
        this.devices[device.name] = Object.assign(new EmbeddedDevice_1.default(), {
            name: device.name,
            status: device.status,
            type: device.type,
            programmer: device.programmer,
            location: 'remote',
            host: rootUrl,
            info: device.info
        });
        // TODO: use a blackbox-service function to do this and all other __path references
        var path = this.devices[device.name].__path;
        if (path && !/^\w+:\/\//.test(path)) { // make sure it's absolute
            this.devices[device.name].__path = rootUrl + path;
        }
        // const links = (<any>device).links
        // if(links && links.self) {
        //   if(!(<any>this.devices[device.name]).links) {
        //     (<any>this.devices[device.name]).links = {}
        //   }
        //   if(/^\w+:\/\//.test(links.self.href)) { // Is the URL absolute?
        //     (<any>this.devices[device.name]).links.self = links.self
        //   } else {
        //     (<any>this.devices[device.name]).links.self = {
        //       href: rootUrl + links.self.href // Make the path absolute.
        //     }
        //   }
        // }
    };
    EmbeddedServiceImpl.prototype.updateInfo = function (device, req) {
        var _a, _b, _c;
        var getInfo = (_a = device.module) === null || _a === void 0 ? void 0 : _a[(_c = (_b = device.driver) === null || _b === void 0 ? void 0 : _b.getInfo) !== null && _c !== void 0 ? _c : 'getInfo'];
        if (typeof getInfo === 'function') {
            Promise.resolve(getInfo({ req: req })).then(function (info) { return device.info = info; });
        }
        return device;
    };
    EmbeddedServiceImpl.prototype.getEmbeddedList = function (_a) {
        var _this = this;
        var req = _a.req;
        return Object.values(this.devices).map(function (device) { return _this.updateInfo(device, req); });
    };
    EmbeddedServiceImpl.prototype.getOptionsForEmbeddedService = function (props) {
        return blackbox_services_1.makeServiceObject(this.oasDoc, 'embedded');
    };
    EmbeddedServiceImpl.prototype.getEmbedded = function (_a) {
        var name = _a.name, req = _a.req;
        return this.updateInfo(this.devices[name], req);
    };
    EmbeddedServiceImpl.prototype.getOptionsForEmbedded = function (props) {
        return blackbox_services_1.makeServiceObject(this.oasDoc, 'embedded');
    };
    EmbeddedServiceImpl.prototype.replaceEmbedded = function (props) {
        return this.updateEmbedded(props);
    };
    EmbeddedServiceImpl.prototype.setDeviceError = function (device, err) {
        device.status = 'error';
        console.error(err);
    };
    EmbeddedServiceImpl.prototype.updateEmbedded = function (_a) {
        var _this = this;
        var _b, _c, _d, _e, _f, _g, _h, _j, _k, _l;
        var name = _a.name, embeddedDevice = _a.embeddedDevice;
        var device = this.devices[name];
        if (!device) {
            throw new Error("Device with name " + name + " not found.");
        }
        if (device.location === 'remote') {
            throw new Error("Cannot update remote device.");
        }
        switch (embeddedDevice.status) {
            case "starting":
            case "running":
                if (device.status === "stopped") {
                    var state = { owner: embeddedDevice.owner };
                    console.log("Starting device " + embeddedDevice.name + " for owner " + ((_b = embeddedDevice.owner) === null || _b === void 0 ? void 0 : _b.username) + " with state " + JSON.stringify(state)); // TODO: Validate credentials.
                    device.status = "starting";
                    // Start device:
                    if (typeof ((_c = device.module) === null || _c === void 0 ? void 0 : _c[(_e = (_d = device.driver) === null || _d === void 0 ? void 0 : _d.onStart) !== null && _e !== void 0 ? _e : 'onStart']) === 'function') {
                        Promise.resolve(device.module[(_g = (_f = device.driver) === null || _f === void 0 ? void 0 : _f.onStart) !== null && _g !== void 0 ? _g : 'onStart'](state))
                            .then(function () { return device.status = 'running'; })
                            .catch(function (err) { return _this.setDeviceError(device, err); });
                    }
                }
                break;
            case "stopped":
                if (device.status !== "stopped") {
                    device.status = 'stopping';
                    // Stop device:
                    if (typeof device.module[(_j = (_h = device.driver) === null || _h === void 0 ? void 0 : _h.onStop) !== null && _j !== void 0 ? _j : 'onStop'] === 'function') {
                        Promise.resolve(device.module[(_l = (_k = device.driver) === null || _k === void 0 ? void 0 : _k.onStop) !== null && _l !== void 0 ? _l : 'onStop']())
                            .then(function () { return device.status = 'stopped'; })
                            .catch(function (err) { return _this.setDeviceError(device, err); });
                    }
                    // TODO: handle port with driver module and getInfo
                    // if(device.programmer) {
                    //   device.programmer.port = undefined
                    // }
                }
                break;
        }
        return { name: name };
    };
    __decorate([
        blackbox_ioc_1.autowired('oasDoc')
    ], EmbeddedServiceImpl.prototype, "oasDoc", void 0);
    __decorate([
        blackbox_ioc_1.autowired('agent')
    ], EmbeddedServiceImpl.prototype, "agent", void 0);
    __decorate([
        blackbox_ioc_1.autowired('blackbox-config')
    ], EmbeddedServiceImpl.prototype, "config", void 0);
    __decorate([
        blackbox_ioc_2.taggedService('init', 'init-devices')
    ], EmbeddedServiceImpl.prototype, "initDevices", null);
    __decorate([
        blackbox_ioc_2.taggedService('init', 'init-agent')
    ], EmbeddedServiceImpl.prototype, "initAgent", null);
    EmbeddedServiceImpl = __decorate([
        blackbox_ioc_1.serviceClass('embedded-service')
    ], EmbeddedServiceImpl);
    return EmbeddedServiceImpl;
}());
exports.EmbeddedServiceImpl = EmbeddedServiceImpl;
