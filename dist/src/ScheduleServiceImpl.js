"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ScheduleServiceImpl = void 0;
var blackbox_ioc_1 = require("blackbox-ioc");
var blackbox_services_1 = require("blackbox-services");
var fs_1 = __importDefault(require("fs"));
var axios_1 = __importDefault(require("axios"));
var https = require('https');
var DATA_PATH = "scheduleData.json";
var httpEvent = function (httpRequest, config) { return function () {
    var _a, _b;
    var agent = ((_b = (_a = httpRequest.url) === null || _a === void 0 ? void 0 : _a.toLowerCase()) === null || _b === void 0 ? void 0 : _b.startsWith('https')) ?
        new https.Agent({
            rejectUnauthorized: false // TODO: Replace this with a ca property pointing to trusted certificates.
        }) :
        undefined;
    axios_1.default.request({
        method: httpRequest.method,
        url: httpRequest.url,
        headers: Object.assign({ 'content-type': 'application/json' }, (httpRequest.headers)),
        data: httpRequest.body,
        httpsAgent: agent
    })
        .then(function (_response) { return console.log("Event sent to " + httpRequest.url); })
        .catch(function (error) {
        console.error("Error sending " + httpRequest.method + " request to " + httpRequest.url);
        console.error(error);
    });
}; };
var ScheduleServiceImpl = /** @class */ (function () {
    function ScheduleServiceImpl() {
        var _this = this;
        this.data = [];
        this.eventHandles = {};
        if (fs_1.default.existsSync(DATA_PATH)) {
            try {
                this.data = JSON.parse(fs_1.default.readFileSync(DATA_PATH).toString());
                this.data.forEach(function (s) { return _this.scheduleEvents(s); });
            }
            catch (err) {
                console.error(err);
            }
        }
    }
    ScheduleServiceImpl.prototype.writeData = function () {
        fs_1.default.promises.writeFile(DATA_PATH, JSON.stringify(this.data, null, 2))
            .catch(console.error);
    };
    ScheduleServiceImpl.prototype.scheduleEvents = function (schedule) {
        var _this = this;
        var _a;
        this.eventHandles[schedule.name] = [];
        (_a = schedule.events) === null || _a === void 0 ? void 0 : _a.forEach(function (event) {
            var endTime = new Date(schedule.end).getTime();
            var startTime = new Date(schedule.start).getTime();
            var now = new Date().getTime();
            var delay;
            if (event.trigger === 'start') {
                // If end is in the future but start is in the past then trigger now:
                if (startTime < now && now < endTime) {
                    delay = 0;
                }
                else {
                    delay = startTime - now;
                }
            }
            else {
                delay = endTime - now;
            }
            if (delay >= 0) {
                var eventFn = _this.createEvent(event);
                if (eventFn) {
                    _this.eventHandles[schedule.name].push(setTimeout(eventFn, delay));
                }
            }
        });
    };
    ScheduleServiceImpl.prototype.createEvent = function (event) {
        if ((event === null || event === void 0 ? void 0 : event.type) === 'http' && event.httpRequest) {
            return httpEvent(event.httpRequest, this.config);
        }
        return undefined;
    };
    ScheduleServiceImpl.prototype.cancelEvents = function (schedule) {
        var _a;
        // Cancel all schedules:
        this.eventHandles[schedule.name].forEach(clearTimeout);
        // Trigger schedule end if cancelling a schedule in progress:
        var now = new Date().getTime();
        var startTime = new Date(schedule.start).getTime();
        var endTime = new Date(schedule.end).getTime();
        if (startTime < now && now < endTime) {
            var eventFn = this.createEvent((_a = schedule.events) === null || _a === void 0 ? void 0 : _a.find(function (event) { return event.trigger === 'end'; }));
            if (eventFn) {
                eventFn();
            }
        }
    };
    ScheduleServiceImpl.prototype.findSchedule = function (name) {
        var filtered = this.data.filter(function (s) { return s.name === name; });
        if (filtered.length > 0) {
            return filtered[0];
        }
        else {
            return undefined;
        }
    };
    ScheduleServiceImpl.prototype.getScheduleList = function (props) {
        return this.data;
    };
    ScheduleServiceImpl.prototype.getOptionsForScheduleService = function (props) {
        return blackbox_services_1.makeServiceObject(this.oasDoc, 'schedule');
    };
    ScheduleServiceImpl.prototype.createSchedule = function (_a) {
        var schedule = _a.schedule;
        this.data.push(schedule);
        this.scheduleEvents(schedule);
        this.writeData();
        return { name: schedule.name };
    };
    ScheduleServiceImpl.prototype.getSchedule = function (_a) {
        var name = _a.name;
        return this.findSchedule(name);
    };
    ScheduleServiceImpl.prototype.getOptionsForSchedule = function (props) {
        return blackbox_services_1.makeServiceObject(this.oasDoc, 'schedule');
    };
    ScheduleServiceImpl.prototype.replaceSchedule = function (props) {
        return this.updateSchedule(props);
    };
    ScheduleServiceImpl.prototype.updateSchedule = function (_a) {
        var schedule = _a.schedule, name = _a.name;
        var s = this.findSchedule(name);
        if (s) {
            this.cancelEvents(s);
            Object.assign(s, schedule);
            this.scheduleEvents(s);
            this.writeData();
            return { name: name };
        }
        return undefined;
    };
    ScheduleServiceImpl.prototype.deleteSchedule = function (_a) {
        var name = _a.name;
        var schedule = this.findSchedule(name);
        if (schedule) {
            this.cancelEvents(schedule);
            for (var i = 0; i < this.data.length; ++i) {
                if (this.data[i].name === name) {
                    this.data.splice(i, 1);
                    this.writeData();
                    return { name: name };
                }
            }
        }
        return undefined;
    };
    __decorate([
        blackbox_ioc_1.autowired('oasDoc')
    ], ScheduleServiceImpl.prototype, "oasDoc", void 0);
    __decorate([
        blackbox_ioc_1.autowired('blackbox-config')
    ], ScheduleServiceImpl.prototype, "config", void 0);
    ScheduleServiceImpl = __decorate([
        blackbox_ioc_1.serviceClass('schedule-service')
    ], ScheduleServiceImpl);
    return ScheduleServiceImpl;
}());
exports.ScheduleServiceImpl = ScheduleServiceImpl;
