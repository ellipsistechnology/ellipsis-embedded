'use strict';
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
function verbose(log) {
    console.log(log);
}
var blackbox_ioc_1 = require("blackbox-ioc");
var blackbox_rules_utils_1 = require("blackbox-rules-utils");
var cors_1 = __importDefault(require("cors"));
var uuid_1 = require("uuid");
var blackbox_discovery_1 = require("blackbox-discovery");
var DEFAULT_PATH = process.env.HOME + '/.blackbox';
var CONFIG_NAME = 'blackbox-conf.json';
var CONFIG_PATHS = [DEFAULT_PATH + '/' + CONFIG_NAME, '/etc/' + CONFIG_NAME];
var DEFAULT_PORT = 8100;
process
    .on('unhandledRejection', function (reason, p) {
    console.error(reason, 'Unhandled Rejection at Promise', p);
})
    .on('uncaughtException', function (err) {
    console.error(err, 'Uncaught Exception thrown');
});
var fs = require('fs'), path = require('path'), https = require('https'), http = require('http'), glob = require('glob'), path = require('path'), app = require('connect')(), oas3Tools = require('oas3-tools'), jsyaml = require('js-yaml');
// Load all files to ensure annotations are read:
glob
    .sync(__dirname + "/*src/**/*.js")
    .forEach(function (file) {
    require(path.resolve(file));
});
// The Swagger document (require it, build it programmatically, fetch it from a URL, ...)
var spec = fs.readFileSync(path.join(__dirname, 'api/openapi.yaml'), 'utf8');
var swaggerDoc = jsyaml.safeLoad(spec);
// Read or setup config:
var config = undefined;
for (var _i = 0, CONFIG_PATHS_1 = CONFIG_PATHS; _i < CONFIG_PATHS_1.length; _i++) {
    var strPath = CONFIG_PATHS_1[_i];
    var p = path.resolve(strPath);
    verbose("looking for config in path " + p);
    if (fs.existsSync(p)) {
        verbose("found config...");
        config = JSON.parse(fs.readFileSync(p).toString());
        if (config) {
            verbose(config);
            if (!config.agent) {
                throw new Error("Blackbox config found at path '" + strPath + "' but agent element not found.");
            }
            if (process.argv[2]) {
                config.agent.name = process.argv[2];
            }
            else if (!config.agent.name) {
                throw new Error("Blackbox config found at path '" + strPath + "' but agent name not found.");
            }
            if (process.argv[3]) {
                config.agent.port = parseInt(process.argv[3]);
            }
            else if (!config.agent.port) {
                config.agent.port = DEFAULT_PORT;
            }
            break; // found a config, don't need to look for others
        }
        else {
            verbose("failed to load config from '" + strPath);
        }
    }
}
// Create default config:
if (!config) {
    verbose("config not found; creating default config:");
    config = {
        agent: {
            name: (_a = process.argv[2]) !== null && _a !== void 0 ? _a : uuid_1.v4(),
            port: process.argv[3] ? parseInt(process.argv[3]) : DEFAULT_PORT
        }
    };
    verbose(config);
    fs.mkdirSync(path.resolve(DEFAULT_PATH), { recursive: true });
    var configPath = path.resolve(DEFAULT_PATH + '/' + CONFIG_NAME);
    fs.writeFileSync(configPath, JSON.stringify(config, null, 2));
    verbose("config created at path " + configPath);
}
// Initialisation:
var _InitIoc = /** @class */ (function () {
    function _InitIoc() {
    }
    _InitIoc.prototype.createRuleBase = function () {
        return new blackbox_rules_utils_1.DefaultRuleBase();
    };
    _InitIoc.prototype.getOasDoc = function () {
        return swaggerDoc;
    };
    _InitIoc.prototype.getConfig = function () {
        return config;
    };
    _InitIoc.prototype.createAgent = function () {
        var _a;
        var agent = new blackbox_discovery_1.Agent({
            name: config.agent.name,
            path: "/",
            port: config.agent.port,
            protocol: (_a = config.agent.protocol) !== null && _a !== void 0 ? _a : 'http'
        });
        agent.startAnnouncing();
        return agent;
    };
    _InitIoc.prototype.initServer = function () {
        // swaggerRouter configuration
        var options = {
            swaggerUi: path.join(__dirname, '/openapi.json'),
            controllers: path.join(__dirname, './gensrc/controllers'),
            useStubs: process.env.NODE_ENV === 'development' // Conditionally turn on stubs (mock mode)
        };
        // Initialize the Swagger middleware
        oas3Tools.initializeMiddleware(swaggerDoc, function (middleware) {
            app.use(cors_1.default());
            // Interpret Swagger resources and attach metadata to request - must be first in swagger-tools middleware chain
            app.use(middleware.swaggerMetadata());
            // Validate Swagger requests
            app.use(middleware.swaggerValidator());
            // Route validated requests to appropriate controller
            app.use(middleware.swaggerRouter(options));
            // Serve the Swagger documents and Swagger UI
            app.use(middleware.swaggerUi());
            // Start the server
            (config.agent.tls ?
                https.createServer({
                    key: fs.readFileSync(config.agent.tls.key),
                    cert: fs.readFileSync(config.agent.tls.cert),
                    ca: config.agent.tls.ca ? fs.readFileSync(config.agent.tls.ca) : undefined,
                }, app) :
                http.createServer(app)).listen(config.agent.port, function () {
                console.log("Your server is listening on port " + config.agent.port + " (" + (config.agent.tls ? 'https' : 'http') + "://localhost:" + config.agent.port + ")");
                console.log("Swagger-ui is available on " + (config.agent.tls ? 'https' : 'http') + "://localhost:" + config.agent.port + "/docs");
            });
        });
    };
    __decorate([
        blackbox_ioc_1.factory('rulebase')
    ], _InitIoc.prototype, "createRuleBase", null);
    __decorate([
        blackbox_ioc_1.factory('oasDoc')
    ], _InitIoc.prototype, "getOasDoc", null);
    __decorate([
        blackbox_ioc_1.factory('blackbox-config')
    ], _InitIoc.prototype, "getConfig", null);
    __decorate([
        blackbox_ioc_1.factory('agent')
    ], _InitIoc.prototype, "createAgent", null);
    __decorate([
        blackbox_ioc_1.taggedService('init', 'init-server')
    ], _InitIoc.prototype, "initServer", null);
    return _InitIoc;
}());
var blackbox_rules_service_1 = __importDefault(require("blackbox-rules-service"));
blackbox_rules_service_1.default();
var blackbox_root_service_1 = __importDefault(require("blackbox-root-service"));
blackbox_root_service_1.default();
blackbox_ioc_1.callServices('init');
