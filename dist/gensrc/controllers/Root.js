'use strict';
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var utils = require('../utils/writer.js');
var blackbox_ioc_1 = require("blackbox-ioc");
var RootServiceWrapper = /** @class */ (function () {
    function RootServiceWrapper() {
    }
    __decorate([
        blackbox_ioc_1.autowiredService('root-service')
    ], RootServiceWrapper.prototype, "service", void 0);
    return RootServiceWrapper;
}());
var rootWrapper = new RootServiceWrapper();
function strip__(target) {
    if (Array.isArray(target)) {
        return target.map(strip__);
    }
    else if (target && typeof target === 'object') {
        var ret_1 = {};
        Object.keys(target).forEach(function (key) {
            if (!key.startsWith('__'))
                ret_1[key] = target[key];
        });
        return ret_1;
    }
    else {
        return target;
    }
}
function convertBooleanParameter(val) {
    if (val === 'false')
        return false;
    if (val === '')
        return true;
    return val;
}
// 
// var Root = require('../service/RootService');
// 
module.exports.getRootService = function getRootService(req, res, next) {
    try {
        Promise.resolve(rootWrapper.service.getRootService({ req: req }))
            .then(function (val) {
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
