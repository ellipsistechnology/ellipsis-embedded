'use strict';
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var utils = require('../utils/writer.js');
var blackbox_ioc_1 = require("blackbox-ioc");
var blackbox_services_1 = require("blackbox-services");
var EmbeddedServiceWrapper = /** @class */ (function () {
    function EmbeddedServiceWrapper() {
    }
    __decorate([
        blackbox_ioc_1.autowiredService('embedded-service')
    ], EmbeddedServiceWrapper.prototype, "service", void 0);
    return EmbeddedServiceWrapper;
}());
var embeddedWrapper = new EmbeddedServiceWrapper();
function strip__(target) {
    if (Array.isArray(target)) {
        return target.map(strip__);
    }
    else if (target && typeof target === 'object') {
        var ret_1 = {};
        Object.keys(target).forEach(function (key) {
            if (!key.startsWith('__'))
                ret_1[key] = target[key];
        });
        return ret_1;
    }
    else {
        return target;
    }
}
function convertBooleanParameter(val) {
    if (val === 'false')
        return false;
    if (val === '')
        return true;
    return val;
}
// 
// var Embedded = require('../service/EmbeddedService');
// 
module.exports.getEmbedded = function getEmbedded(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var depth = req.swagger.params['depth'].value;
    verbose = convertBooleanParameter(verbose);
    try {
        Promise.resolve(embeddedWrapper.service.getEmbedded({ req: req, name: name, verbose: verbose, depth: depth }))
            .then(function (val) {
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = blackbox_services_1.trimToDepth(val, depth);
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
module.exports.getEmbeddedList = function getEmbeddedList(req, res, next) {
    var depth = req.swagger.params['depth'].value;
    var verbose = req.swagger.params['verbose'].value;
    verbose = convertBooleanParameter(verbose);
    try {
        Promise.resolve(embeddedWrapper.service.getEmbeddedList({ req: req, depth: depth, verbose: verbose }))
            .then(function (val) {
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = blackbox_services_1.trimToDepth(val, depth);
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
module.exports.getOptionsForEmbedded = function getOptionsForEmbedded(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var depth = req.swagger.params['depth'].value;
    verbose = convertBooleanParameter(verbose);
    try {
        Promise.resolve(embeddedWrapper.service.getOptionsForEmbedded({ req: req, name: name, verbose: verbose, depth: depth }))
            .then(function (val) {
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = blackbox_services_1.trimToDepth(val, depth);
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
module.exports.getOptionsForEmbeddedService = function getOptionsForEmbeddedService(req, res, next) {
    var depth = req.swagger.params['depth'].value;
    var verbose = req.swagger.params['verbose'].value;
    verbose = convertBooleanParameter(verbose);
    try {
        Promise.resolve(embeddedWrapper.service.getOptionsForEmbeddedService({ req: req, depth: depth, verbose: verbose }))
            .then(function (val) {
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = blackbox_services_1.trimToDepth(val, depth);
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
module.exports.replaceEmbedded = function replaceEmbedded(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var embeddedDevice = req.body;
    verbose = convertBooleanParameter(verbose);
    try {
        Promise.resolve(embeddedWrapper.service.replaceEmbedded({ req: req, name: name, verbose: verbose, embeddedDevice: embeddedDevice }))
            .then(function (val) {
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
module.exports.updateEmbedded = function updateEmbedded(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var embeddedDevice = req.body;
    verbose = convertBooleanParameter(verbose);
    try {
        Promise.resolve(embeddedWrapper.service.updateEmbedded({ req: req, name: name, verbose: verbose, embeddedDevice: embeddedDevice }))
            .then(function (val) {
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
