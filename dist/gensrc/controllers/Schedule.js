'use strict';
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var utils = require('../utils/writer.js');
var blackbox_ioc_1 = require("blackbox-ioc");
var blackbox_services_1 = require("blackbox-services");
var ScheduleServiceWrapper = /** @class */ (function () {
    function ScheduleServiceWrapper() {
    }
    __decorate([
        blackbox_ioc_1.autowiredService('schedule-service')
    ], ScheduleServiceWrapper.prototype, "service", void 0);
    return ScheduleServiceWrapper;
}());
var scheduleWrapper = new ScheduleServiceWrapper();
function strip__(target) {
    if (Array.isArray(target)) {
        return target.map(strip__);
    }
    else if (target && typeof target === 'object') {
        var ret_1 = {};
        Object.keys(target).forEach(function (key) {
            if (!key.startsWith('__'))
                ret_1[key] = target[key];
        });
        return ret_1;
    }
    else {
        return target;
    }
}
function convertBooleanParameter(val) {
    if (val === 'false')
        return false;
    if (val === '')
        return true;
    return val;
}
// 
// var Schedule = require('../service/ScheduleService');
// 
module.exports.createSchedule = function createSchedule(req, res, next) {
    var verbose = req.swagger.params['verbose'].value;
    var schedule = req.body;
    verbose = convertBooleanParameter(verbose);
    try {
        Promise.resolve(scheduleWrapper.service.createSchedule({ req: req, verbose: verbose, schedule: schedule }))
            .then(function (val) {
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
module.exports.deleteSchedule = function deleteSchedule(req, res, next) {
    var name = req.swagger.params['name'].value;
    try {
        Promise.resolve(scheduleWrapper.service.deleteSchedule({ req: req, name: name }))
            .then(function (val) {
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
module.exports.getOptionsForSchedule = function getOptionsForSchedule(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var depth = req.swagger.params['depth'].value;
    verbose = convertBooleanParameter(verbose);
    try {
        Promise.resolve(scheduleWrapper.service.getOptionsForSchedule({ req: req, name: name, verbose: verbose, depth: depth }))
            .then(function (val) {
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = blackbox_services_1.trimToDepth(val, depth);
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
module.exports.getOptionsForScheduleService = function getOptionsForScheduleService(req, res, next) {
    var depth = req.swagger.params['depth'].value;
    var verbose = req.swagger.params['verbose'].value;
    verbose = convertBooleanParameter(verbose);
    try {
        Promise.resolve(scheduleWrapper.service.getOptionsForScheduleService({ req: req, depth: depth, verbose: verbose }))
            .then(function (val) {
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = blackbox_services_1.trimToDepth(val, depth);
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
module.exports.getSchedule = function getSchedule(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var depth = req.swagger.params['depth'].value;
    verbose = convertBooleanParameter(verbose);
    try {
        Promise.resolve(scheduleWrapper.service.getSchedule({ req: req, name: name, verbose: verbose, depth: depth }))
            .then(function (val) {
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = blackbox_services_1.trimToDepth(val, depth);
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
module.exports.getScheduleList = function getScheduleList(req, res, next) {
    var depth = req.swagger.params['depth'].value;
    var verbose = req.swagger.params['verbose'].value;
    verbose = convertBooleanParameter(verbose);
    try {
        Promise.resolve(scheduleWrapper.service.getScheduleList({ req: req, depth: depth, verbose: verbose }))
            .then(function (val) {
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = blackbox_services_1.trimToDepth(val, depth);
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
module.exports.replaceSchedule = function replaceSchedule(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var schedule = req.body;
    verbose = convertBooleanParameter(verbose);
    try {
        Promise.resolve(scheduleWrapper.service.replaceSchedule({ req: req, name: name, verbose: verbose, schedule: schedule }))
            .then(function (val) {
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
module.exports.updateSchedule = function updateSchedule(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var schedule = req.body;
    verbose = convertBooleanParameter(verbose);
    try {
        Promise.resolve(scheduleWrapper.service.updateSchedule({ req: req, name: name, verbose: verbose, schedule: schedule }))
            .then(function (val) {
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
