'use strict';
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var utils = require('../utils/writer.js');
var blackbox_ioc_1 = require("blackbox-ioc");
var blackbox_services_1 = require("blackbox-services");
var RulebaseServiceWrapper = /** @class */ (function () {
    function RulebaseServiceWrapper() {
    }
    __decorate([
        blackbox_ioc_1.autowiredService('rulebase-service')
    ], RulebaseServiceWrapper.prototype, "service", void 0);
    return RulebaseServiceWrapper;
}());
var rulebaseWrapper = new RulebaseServiceWrapper();
function strip__(target) {
    if (Array.isArray(target)) {
        return target.map(strip__);
    }
    else if (target && typeof target === 'object') {
        var ret_1 = {};
        Object.keys(target).forEach(function (key) {
            if (!key.startsWith('__'))
                ret_1[key] = target[key];
        });
        return ret_1;
    }
    else {
        return target;
    }
}
function convertBooleanParameter(val) {
    if (val === 'false')
        return false;
    if (val === '')
        return true;
    return val;
}
// 
// var Rulebase = require('../service/RulebaseService');
// 
module.exports.createCondition = function createCondition(req, res, next) {
    var verbose = req.swagger.params['verbose'].value;
    var condition = req.body;
    verbose = convertBooleanParameter(verbose);
    try {
        Promise.resolve(rulebaseWrapper.service.createCondition({ req: req, verbose: verbose, condition: condition }))
            .then(function (val) {
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
module.exports.createRule = function createRule(req, res, next) {
    var verbose = req.swagger.params['verbose'].value;
    var rule = req.body;
    verbose = convertBooleanParameter(verbose);
    try {
        Promise.resolve(rulebaseWrapper.service.createRule({ req: req, verbose: verbose, rule: rule }))
            .then(function (val) {
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
module.exports.createValue = function createValue(req, res, next) {
    var verbose = req.swagger.params['verbose'].value;
    var value = req.body;
    verbose = convertBooleanParameter(verbose);
    try {
        Promise.resolve(rulebaseWrapper.service.createValue({ req: req, verbose: verbose, value: value }))
            .then(function (val) {
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
module.exports.deleteCondition = function deleteCondition(req, res, next) {
    var name = req.swagger.params['name'].value;
    try {
        Promise.resolve(rulebaseWrapper.service.deleteCondition({ req: req, name: name }))
            .then(function (val) {
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
module.exports.deleteRule = function deleteRule(req, res, next) {
    var name = req.swagger.params['name'].value;
    try {
        Promise.resolve(rulebaseWrapper.service.deleteRule({ req: req, name: name }))
            .then(function (val) {
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
module.exports.deleteValue = function deleteValue(req, res, next) {
    var name = req.swagger.params['name'].value;
    try {
        Promise.resolve(rulebaseWrapper.service.deleteValue({ req: req, name: name }))
            .then(function (val) {
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
module.exports.getCondition = function getCondition(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var depth = req.swagger.params['depth'].value;
    verbose = convertBooleanParameter(verbose);
    try {
        Promise.resolve(rulebaseWrapper.service.getCondition({ req: req, name: name, verbose: verbose, depth: depth }))
            .then(function (val) {
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = blackbox_services_1.trimToDepth(val, depth);
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
module.exports.getConditions = function getConditions(req, res, next) {
    var verbose = req.swagger.params['verbose'].value;
    var depth = req.swagger.params['depth'].value;
    verbose = convertBooleanParameter(verbose);
    try {
        Promise.resolve(rulebaseWrapper.service.getConditions({ req: req, verbose: verbose, depth: depth }))
            .then(function (val) {
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = blackbox_services_1.trimToDepth(val, depth);
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
module.exports.getRule = function getRule(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var depth = req.swagger.params['depth'].value;
    verbose = convertBooleanParameter(verbose);
    try {
        Promise.resolve(rulebaseWrapper.service.getRule({ req: req, name: name, verbose: verbose, depth: depth }))
            .then(function (val) {
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = blackbox_services_1.trimToDepth(val, depth);
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
module.exports.getRulebaseService = function getRulebaseService(req, res, next) {
    try {
        Promise.resolve(rulebaseWrapper.service.getRulebaseService({ req: req }))
            .then(function (val) {
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
module.exports.getRules = function getRules(req, res, next) {
    var verbose = req.swagger.params['verbose'].value;
    var depth = req.swagger.params['depth'].value;
    verbose = convertBooleanParameter(verbose);
    try {
        Promise.resolve(rulebaseWrapper.service.getRules({ req: req, verbose: verbose, depth: depth }))
            .then(function (val) {
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = blackbox_services_1.trimToDepth(val, depth);
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
module.exports.getValue = function getValue(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var depth = req.swagger.params['depth'].value;
    verbose = convertBooleanParameter(verbose);
    try {
        Promise.resolve(rulebaseWrapper.service.getValue({ req: req, name: name, verbose: verbose, depth: depth }))
            .then(function (val) {
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = blackbox_services_1.trimToDepth(val, depth);
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
module.exports.getValues = function getValues(req, res, next) {
    var verbose = req.swagger.params['verbose'].value;
    var depth = req.swagger.params['depth'].value;
    verbose = convertBooleanParameter(verbose);
    try {
        Promise.resolve(rulebaseWrapper.service.getValues({ req: req, verbose: verbose, depth: depth }))
            .then(function (val) {
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = blackbox_services_1.trimToDepth(val, depth);
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
module.exports.replaceCondition = function replaceCondition(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var condition = req.body;
    verbose = convertBooleanParameter(verbose);
    try {
        Promise.resolve(rulebaseWrapper.service.replaceCondition({ req: req, name: name, verbose: verbose, condition: condition }))
            .then(function (val) {
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
module.exports.replaceRule = function replaceRule(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var rule = req.body;
    verbose = convertBooleanParameter(verbose);
    try {
        Promise.resolve(rulebaseWrapper.service.replaceRule({ req: req, name: name, verbose: verbose, rule: rule }))
            .then(function (val) {
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
module.exports.replaceValue = function replaceValue(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var value = req.body;
    verbose = convertBooleanParameter(verbose);
    try {
        Promise.resolve(rulebaseWrapper.service.replaceValue({ req: req, name: name, verbose: verbose, value: value }))
            .then(function (val) {
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
module.exports.updateCondition = function updateCondition(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var condition = req.body;
    verbose = convertBooleanParameter(verbose);
    try {
        Promise.resolve(rulebaseWrapper.service.updateCondition({ req: req, name: name, verbose: verbose, condition: condition }))
            .then(function (val) {
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
module.exports.updateRule = function updateRule(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var rule = req.body;
    verbose = convertBooleanParameter(verbose);
    try {
        Promise.resolve(rulebaseWrapper.service.updateRule({ req: req, name: name, verbose: verbose, rule: rule }))
            .then(function (val) {
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
module.exports.updateValue = function updateValue(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var value = req.body;
    verbose = convertBooleanParameter(verbose);
    try {
        Promise.resolve(rulebaseWrapper.service.updateValue({ req: req, name: name, verbose: verbose, value: value }))
            .then(function (val) {
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = strip__(val);
            if (req.swagger &&
                req.swagger.operation &&
                req.swagger.operation.responses &&
                req.swagger.operation.responses['200'] &&
                req.swagger.operation.responses['200']['x-content-blackbox-type']) {
                var contentBlackboxType = req.swagger.operation.responses['200']['x-content-blackbox-type'];
                res.setHeader('content-blackbox-type', contentBlackboxType);
            }
            utils.writeJson(res, val);
        }).catch(function (err) {
            next(err);
        });
    }
    catch (err) {
        next(err);
    }
};
